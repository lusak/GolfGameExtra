﻿using UnityEngine;
using System.Collections;

public class BallDirectlyIntoHole : Ball
{
	[SerializeField] private Collider2D groundCollider;

	void Update()
	{
		base.Update();

		if (launched && collider.IsTouching(holeCollider) && rBody.velocity.magnitude < ballAssumedStopedVelocity)
		{
			LevelWon();
		}

		if (!levelWon && launched && collider.IsTouching(groundCollider))
		{
			LevelManager.instance.GameOver();
		}
	}
}