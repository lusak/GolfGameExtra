﻿using UnityEngine;
using System.Collections;

public class BallCanRoll : Ball
{
	void Update()
	{
		base.Update();

		if (launched && rBody.velocity.magnitude < ballAssumedStopedVelocity)
		{
			if (collider.IsTouching(holeCollider))
			{
				LevelWon();
			}
			//When ball hits the edge of the hole, sometimes it slows down to almost zero, and then...
			//after a fraction of a second it falls into hole. That's why we need this check
			else if (!checkingLevelFailed)
			{
				checkingLevelFailed = true;
				StartCoroutine(MakeSureLevelFailed());
			}
		}
	}
}