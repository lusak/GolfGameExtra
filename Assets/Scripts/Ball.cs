﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class Ball : MonoBehaviour {

	[SerializeField] protected Collider2D holeCollider;
	[SerializeField] protected float ballAssumedStopedVelocity = 0.02f;
	
	[SerializeField] private GameObject ballProjectionPrefab;
	[SerializeField] private GameObject ballProjectionsParent;

	[SerializeField] private float velocityLaunchingIncrease = 0.05f;
	[SerializeField] private float levelCompleteVelocityIncrease = 0.01f;

	[SerializeField] private float angleLaunchingIncrese = 0.2f;
	[SerializeField] private float levelCompleteAngleIncrese = 0.04f;

	[SerializeField] private int numberOfBallProjections = 9;

	protected Rigidbody2D rBody;
	protected Collider2D collider;

	//1 degree is around 0,0174 rad. 1 rad is around 57,295 degrees
	protected bool launched = false;
	protected bool checkingLevelFailed = false;
	protected bool levelWon = false;
	
	private bool launching = false;
	private int timesLaunchButtonClicked = 0;
	private float angleToRadsFactor = 0.0174f;
	private float gravityForce;
	private float xScreenEndPosition;
	private float timeAfterWhichBallIsAssumedLaunched = 0.1f;
	private float timeBetweenProjectionsDrawed = 0.03f;

	private List<GameObject> ballProjections = new List<GameObject>();
	private Camera camera;

	protected void Start() {
		rBody = GetComponent<Rigidbody2D>();
		collider = GetComponent<Collider2D>();
		gravityForce = Mathf.Abs(Physics2D.gravity.y);
		camera = Camera.main;
		xScreenEndPosition = GetScreenEndXPosition();
		InstantiateBallProjections();
	}

	protected void Update()
    {
		if (!launched && (Input.GetKeyUp(KeyCode.Space) || CheckIfLastProjectionOutOfScreen()))
		{
			launching = false;
			LaunchBall();
			StartCoroutine(SetToLaunched());
		}

		if (Input.GetKeyDown(KeyCode.Space) && !launched)
		{
			launching = true;
			StartCoroutine(DrawProjection());
		}

		if (Input.GetKey(KeyCode.Space))
		{
			timesLaunchButtonClicked++;
		}
	}

	public void ResetBall()
	{
		rBody.velocity = Vector2.zero;
		launched = false;
		launching = false;
		checkingLevelFailed = false;
		levelWon = false;
		timesLaunchButtonClicked = 0;
		foreach (GameObject ball in ballProjections)
		{
			ball.transform.position = transform.position;
		}
		xScreenEndPosition = GetScreenEndXPosition();
	}

	protected bool CheckIfLastProjectionOutOfScreen()
    {
		if(ballProjections.Count>0)
        {
			//Last ball in the list is the ball furthest away from the ball
			if (ballProjections[ballProjections.Count - 1].transform.position.x > xScreenEndPosition)
			{
				return true;
			}
		}
		return false;
    }

	protected IEnumerator DrawProjection()
    {
		while (launching)
        {
			Vector2[] points = GetTrajectoryPoints();

			int ballProjectionIndex = 0;
			Vector2 ballPosition = transform.position;

			foreach (Vector2 point in points)
			{
				GameObject ballProjection = ballProjections[ballProjectionIndex];
				ballProjection.transform.position = ballPosition + point;
				ballProjectionIndex++;
			}
			yield return new WaitForSeconds(timeBetweenProjectionsDrawed);
		}
    }

	protected float GetScreenEndXPosition()
	{
		float screenWidthInWorldUnits = 2 * camera.orthographicSize * camera.aspect;
		return transform.position.x + screenWidthInWorldUnits;
	}

	protected Vector2[] GetTrajectoryPoints()
    {
		Vector2[] points = new Vector2[numberOfBallProjections];

		float launchAngle = angleLaunchingIncrese * timesLaunchButtonClicked * angleToRadsFactor;
		float launchVelocity = velocityLaunchingIncrease * timesLaunchButtonClicked;

		//Time before ball falls on the ground again
		// t = 2 * v * sin(alfa) / g
		float timeOfFlight = 2 * launchVelocity * Mathf.Abs(Mathf.Sin(launchAngle)) / gravityForce;

		for (int i = 1; i <= numberOfBallProjections; i++)
        {
			//x = v * t * cos(alfa) 
            float x = launchVelocity * timeOfFlight * 1/numberOfBallProjections * i * Mathf.Cos(launchAngle);

			//y = v * t * sin(alfa) - g * t * t / 2
            float y = launchVelocity * timeOfFlight * 1 / numberOfBallProjections * i * Mathf.Sin(launchAngle) 
				- gravityForce / 2 * Mathf.Pow(timeOfFlight * 1 / numberOfBallProjections * i, 2);
			points[i - 1] = new Vector2(x, y);
        }

		return points;
    }

	protected void LevelWon()
	{
		levelWon = true;
		StopAllCoroutines();
		velocityLaunchingIncrease += levelCompleteVelocityIncrease;
		angleLaunchingIncrese += levelCompleteAngleIncrese;
		LevelManager.instance.NewLevel();
	}

	protected void LaunchBall()
	{
		rBody.velocity = GetLaunchVector();
	}

	protected IEnumerator MakeSureLevelFailed()
	{
		yield return new WaitForSeconds(1f);
		if (!collider.IsTouching(holeCollider))
		{
			LevelManager.instance.GameOver();
		}
	}

	protected IEnumerator SetToLaunched()
	{
		yield return new WaitForSeconds(timeAfterWhichBallIsAssumedLaunched);
		launched = true;
	}

	private Vector2 GetLaunchVector()
	{
		float launchVelocity = velocityLaunchingIncrease * timesLaunchButtonClicked;
		float launchAngle = angleLaunchingIncrese * timesLaunchButtonClicked * angleToRadsFactor;
		float xVelocity = launchVelocity * Mathf.Abs(Mathf.Cos(launchAngle));
		float yVelocity = launchVelocity * Mathf.Abs(Mathf.Sin(launchAngle));
		return new Vector2(xVelocity, yVelocity);
	}

	private void InstantiateBallProjections()
	{
		for (int i = 0; i < numberOfBallProjections; i++)
		{
			GameObject ballProjection = Instantiate(ballProjectionPrefab);
			ballProjection.transform.position = transform.position;
			ballProjection.transform.SetParent(ballProjectionsParent.transform);
			ballProjections.Add(ballProjection);
		}
	}
}
